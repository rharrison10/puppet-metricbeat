# @summary This configures a metricbeat module
#
# @param ensure
#   Should the module be configured or removed.
#
# @param config
#   An array with the module configurations.
#
# @example
#   metricbeat::module { 'namevar': }
define metricbeat::module(
  Array                  $config,
  Enum['absent', 'file'] $ensure = 'file',
) {
  file { "/etc/metricbeat/modules.d/${name}.yml":
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => to_yaml($config),
    notify  => Class['metricbeat::service'],
  }
}
