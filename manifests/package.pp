# @summary This class installs the metricbeat package.
#
# @api private
#
class metricbeat::package {
  package { $metricbeat::package_name:
    ensure => $metricbeat::package_ensure,
    source => $metricbeat::package_source,
  }
}
