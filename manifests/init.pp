# metricbeat
#
# @summary
#  Main class, includes all other classes
#
# @param config
#   A hash with the main and output configuration.
#
# @param modules
#   An hash with enabled modules for metricbeat.
#
# @param package_ensure
#   State of package (e.g. `present` or `latest`).
#
# @param package_name
#   Name of the metricbeat package.
#
# @param package_source
#   Optional source location for the package. Use this if you haven't already
#   configured a repo containing the package.
#
# @param service_enable
#   Whether the metricbeat service should be enabled.
#
# @param service_ensure
#   What state should the metricbeat service be in.
#
# @param service_name
#   Name of the metricbeat service.
#
class metricbeat (
  Optional[Hash]            $config         = {},
  Optional[Hash]            $modules        = {},
  Enum['latest', 'present'] $package_ensure = 'present',
  String                    $package_name   = 'metricbeat',
  Optional[String]          $package_source = undef,
  Optional[Boolean]         $service_enable = true,
  Optional[String]          $service_ensure = 'running',
  String                    $service_name   = 'metricbeat',
) {
  contain 'metricbeat::package'
  contain 'metricbeat::config'
  contain 'metricbeat::service'

  Class['metricbeat::package']
  ->Class['metricbeat::config']
  ~>Class['metricbeat::service']
}
