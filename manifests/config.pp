# @summary Handle the metricbeat configuration file and directories.
#
# @api private
#
class metricbeat::config {
  file { '/etc/metricbeat':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { '/etc/metricbeat/modules.d':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    recurse => true,
    purge   => true,
    ignore  => ['*.yml.disabled'],
  }

  file { '/etc/metricbeat/metricbeat.yml':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => epp("${module_name}/metricbeat.yml.epp", {
      'config' => delete(to_yaml($metricbeat::config), "---\n"),
    }),
  }

  $metricbeat::modules.each |String $filename, Array $module_config| {
    metricbeat::module{ $filename:
      config => $module_config,
    }
  }
}
