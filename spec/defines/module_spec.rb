require 'spec_helper'

describe 'metricbeat::module' do
  let(:title) { 'namevar' }
  let(:params) do
    {
      'config' => [],
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:pre_condition) { 'include metricbeat' }

      it { is_expected.to compile }
    end
  end
end
